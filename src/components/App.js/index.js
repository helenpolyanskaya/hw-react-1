import React from "react";
import Button from "../Button";
import Modal from "../Modal";

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            openModal: false,
            modal: {},
        }

        this.modalsData = {
            red: {
                header: "Do you want to delete this file?",
                text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want delete it?",
                actions1: "OK",
                actions2: "Cancel"
            },
            green: {
                header: "Do you want to save it?",
                text: "If not, it will be delete, choose an option:",
                actions1: "Save",
                actions2: "Delete"
            }
        }

        this.clickOpenModal = this.clickOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    clickOpenModal(event) {
        console.log(event.target.dataset.modalId);
        let modalId = event.target.dataset.modalId;
        this.setState({
            modal: this.modalsData[modalId],
            openModal: true
        })
    }

    closeModal() {
        this.setState({
            openModal: false,
        })
    }

    render() {
        return (
            <>
                <div>    
                    <Modal 
                    header = {this.state.modal.header}
                    text = {this.state.modal.text}
                    open = {this.state.openModal} 
                    onClose={this.closeModal}
                    actions1={this.state.modal.actions1}
                    actions2={this.state.modal.actions2}
                    />
                    <h1>Hello Pupsik!</h1>
                    <Button onClick={this.clickOpenModal} backgroundColor='rgb(117, 88, 234)' text = 'Open first modal' modalId="red" class="toggle-button"
                    id="centered-toggle-button"/>
                    <Button onClick={this.clickOpenModal} backgroundColor='rgb(229, 214, 76)' text = 'Open second modal' modalId="green"/>
                </div>
            </>
        )
    }
}

export default App;