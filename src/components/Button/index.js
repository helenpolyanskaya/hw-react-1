//import { render } from "@testing-library/react";
import React from "react";
import './button.css'


class Button extends React.Component {
    constructor(props) {
        super(props);
        
    }
    render(){
        return(
            <button onClick={this.props.onClick} style={
                {backgroundColor: this.props.backgroundColor, padding: '10px 15px'}
            } data-modal-id={this.props.modalId}>{this.props.text}</button>
        )
    }
}

export default Button;